**Mini Exercise 06 by Cecilie Frandsen**

[RunMe](https://glcdn.githack.com/ckf97/my-collection-ap2019/raw/master/MiniEx_06/MiniEx06_library/empty-example/index.html)

![Screenshot](MiniEx06_screenshot.PNG)

**Describe the game and process:**

When the program starts the game begins. Balls will fall from the top and down and you will have to catch them with the white paddle, which is controlled by 
the mouse. The game counts how many balls you have collected. If a ball falls onder the white paddle, the game is over. 

When I programmed the game, I started out with the making the balls fall from the screen, so my first thing to do was make the class and then make it work the 
way I wanted it to. And I did manage to make them fall, and I was able to make it work again as I added the 'start over' button. The real struggle came when 
I had to make the balls bounce off of the white paddle, which initially was what I wanted them to do. I struggled for days to get it to work and was only able 
to make progress when I asked for help on how the balls could even interact with the paddle at all. With help, I could finally make something happen when the 
balls hit the paddle, but I was unable to make them bounce. In my quest to make them bounce, I somehow made them disappear when they hit the paddle instead, but 
I decided to keep it that way in the end. I was tired of looking for the solution to the bounce. 

**Characteristics of Object-Oriented Programming:**
Objects are groupings of data, and object orientation has to do with how the groupings of data can be executed, and what they execute. Object-oriented programming 
is about making computational representations of entities and how the different objects interact with each other. Interaction between individual groups of data 
is what object-oriented programming is all about. 

"*a computer program written in an object-oriented programming language develops a kind of intensional logic, in the sense that each of the objects that the 
program comprises has an internal conceptual structure that determines its relationship to what it ‘refers’ to, suggesting that objects are, in fact concepts, 
concepts that represent objects in a machinic materialization of a logical calculus.*
" (Fuller & Goffey, p. 5)

**Digital Context:**
Object-oriented programming is essentially what all programming is about today. We use it because it is efficient and much easier to work with because objects 
only need to be stated once, and then we simply call them whenever they are needed. But it is also quite tricky because it's difficult to do (especially for 
newbies), and if there's a mistake then it simply won't run. 

Objects are also able to change our perception of things because we have to make computational representations of things we know from real life. And when we 
have to decide which attributes are important enough for us to recognize the object, we risk the chance of changing our perception of that object, what it 
really means or is. Take for example the magnifying glass that represents searching. The magnifying glass is a short line with a circle on top tilted 
to the left or right. Those are the attributes we have decided are enough to recognize it as a magnifying glass, even though a magnifying glass can have many 
different shapes and sizes, and this is now how we will usually recognize a magnifying glass. And it is connected with searching, which some might say is 
similar to its physical use, which is to magnify (to use a more digital word for it, essentially, to zoom in) what is in front of you, so that you can see it 
better. So seeing the magnifying glass as a search object is probably more metaphorical than it is literal.