//Mini Exercise 06 by Cecilie Frandsen
//requirement: use the class syntax (make a game)

let balls = [];
let button;
let playerX = 250;
let playerY = 450;
let counter = 0;

function setup() {
  createCanvas(545, 600);

  //button
  button = createButton('START OVER')
  button.position(50,550);
  button.size(120,20);
  button.mousePressed(startOver);

  addBalls();
}

function draw() {
  //game setting
  background(0);
  noFill();
  stroke(255,255,10);
  rect(20,20,500,500);
  player();

  //show balls
  for (let i = 0; i < balls.length; i++) {
    balls[i].show();
    balls[i].move();

    //game over
    if (balls[i].y > 510) {
      balls[i].y = 510;
      gameOver();
    }
    //player collsion
		if (balls[i].x < playerX+80 && balls[i].x > playerX && balls[i].y > playerY  && balls[i].y < 460) {
			balls[i].disappear();
      counter = counter + 1;
		}
  }
  //point counter
  fill(255);
  textSize(20);
  text(counter, 400, 565);
}

function player() {
  //player
  fill(255);
  noStroke();
  rect(playerX,playerY,80,10);
  //movement
  playerX =+ mouseX;
  playerY =+ 450;
  //borders
  if (playerX < 25) {
    playerX = 25;
  }
  if (playerX > 515-80) {
    playerX = 515-80;
  }
}

function addBalls() {
	for (let i = 0; i < 5; i++) {
    let x = random(40,510);
    let y = 32;
    let r = 10;
    let s = random(1,4);
    balls[i] = new Balls(x,y,r,s);
  }
}

function gameOver() {
  fill(255,0,100);
  textSize(62);
  noStroke();
	text('GAME OVER', 80, height/2);
  noLoop();
}

function startOver() {
  addBalls();
  counter = 0;
  loop();
}

class Balls {
  constructor(xpos, ypos, size, speed) {
    this.x = xpos;
    this.y = ypos;
    this.r = size*2;
    this.speed = speed;
  }

  move() {
    this.y = this.y + this.speed;
  }

  disappear() {
    this.y = this.y * -1;
  }

  show() {
    fill(100,255,100);
    stroke(100,130,100);
    strokeWeight(2);
    ellipse(this.x, this.y, this.r);
  }
}
