//MiniEx02 by Cecilie Frandsen

function setup() {
  createCanvas(windowWidth,windowHeight);
}

function draw() {
  background(220);

  let head = color(185,0,255);
  let eyes = color(0);
  let mouth = color(245);
  let head2 = color(255,255,0);
  let eyes2 = color(255,225,0);
  let mouth2 = color(255,0,0);

  	//head
  fill(head2);
  rect(110,120,200,200,30);
  rect(400,120,200,200,30);
  noStroke();

  	//eyebrows
  fill(255);
  noStroke(0);
  bezier(190,150,160,120,130,150,130,150);
  bezier(230,150,250,120,290,150,290,150);

  	//left head eyes
  fill(eyes);
  rect(150,180,30,20,30);
  rect(240,180,30,20,30);
  	//right head eyes
  rect(440,180,30,20,30);
  rect(530,180,30,20,30);

  	//mouths
  fill(mouth2);
  rect(180,240,60,50,30);
  arc(500,250,70,60,0,PI);

  push();
  	if (mouseX < 600 && mouseX > 110 && mouseY < 320 && mouseY > 120) {
  	//head
  fill(head);
  rect(110,120,200,200,30);
  rect(400,120,200,200,30);
  noStroke();

		//eyebrows
  fill(255);
  noStroke(0);
  bezier(190,180,160,120,130,150,130,150);
  bezier(230,180,250,120,290,150,290,150);

  	//left head eyes
  fill(eyes2);
  rect(150,180,30,20,30);
  rect(240,180,30,20,30);
  	//right head eyes
  rect(445,180,30,20,30);
  rect(530,190,35,10,30);

  	//mouths
  fill(mouth);
  rect(180,240,60,50,30);
  arc(500,250,70,60,0,PI);
  }
  pop();

  push();
  if (mouseIsPressed) {
  	//head
  fill(random(255),0,0);
  rect(110,120,200,200,30);
  rect(400,120,200,200,30);
  noStroke();

    //eyebrows
  fill(255);
  noStroke(0);
  bezier(190,random(150,180),160,120,130,150,130,150);
  bezier(230,random(150,180),250,120,290,150,290,150);

  	//left head eyes
  fill(0,random(255),0);
  rect(150,180,30,20,30);
  rect(240,180,30,20,30);
  	//right head eyes
  rect(445,180,30,20,30);
  rect(530,180,30,20,30);

  	//mouths
  fill(0,0,random(255));
  rect(180,240,60,50,30);
  arc(500,250,70,60,0,PI);
  }
}
