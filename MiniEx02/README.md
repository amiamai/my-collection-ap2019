Mini Exercise 02 by Cecilie Frandsen

[Link](https://cdn.staticaly.com/gl/ckf97/my-collection-ap2019/raw/master/MiniEx02/MiniEx02_library/empty-example/index.html)

My program consists of two emojis showing when the page is loaded. When the cursor moves over the two emojis, they change color and expression. 
The emojis are first traditionally colored in yellow, with black eyes and red mouths, and I decided to make them in rectangles instead of ellipses, 
which made them look kind of like LEGO-heads. When the cursor reaches the emojis, they turn purple with yellow eyes and white/not-colored mouths. 
Only small changes were made to the expressions, twisting eyebrows so they turn down makes the emoji look angry, and for the other one, I just made the emoji wink, 
which makes it a lot more playful than a reagular happy face. I learned how to use the AND (&&) control for this program, and I also tried to make my own variables for colors. 
I had a hard time making the if-function work the way I wanted it to, and ended up starting the project over and going back and forth in the new and old code to figure where it 
went wrong. I'm not sure I actually ended up figuring it out, but I did get it to work the way I wanted it to in the end, so I at least achieved what I wanted. 
I also worked with the bezier-curve-thingy for the first, which was quite difficult and it took a lot of time to get it placed right, but I got it in the end. When working with 
some of the shapes and lines, I constantly have to remind myself to think of the X and Y axis, which for some reason is hard **¯\_(ツ)_/¯**.

I made my emojis yellow when I started, mainly because I wasn't really thinking of specifically making my own emoji, but just making one in general, and generally 
emojis are yellow. But I also realized that I didn't want for my emoji to just look like every other emoji, which is why I went and made them purple if you move the cursor 
over them. I chose to make them purple, because no one's skin is purple and therefor the emoji would be neutral in regards to skin color. In general, I tried to make the 
emojis very neutral, both yellow and purple emojis. There are no definitive characteristics to make the emoji male or female, child or adult, and with the purple also neutral 
in regards to race. If I were to categorize the four emotions, they would be shock, happiness, anger and playfulness, which I think are four basic emotions everyone can 
identify with, and can easily be used to express your feelings in a number of situations. 

I'm not exactly sure if I was trying to make a point with my emojis. I guess I wanted to make the purple ones so I wouldn't only be showing the yellow racially-ambiguous emojis, 
but also emojis that no one can look to and identify with because of how they look but mainly how they're feeling. Which used to be the meaning with the yellow emojis too, 
but in the time since emojis became a thing, a lot has happened to the demographic users as well, mainly becoming very aware of our individuality and diversity. 
I remember drawing as a kid, and if I didn't have a "skin-colored" color pencil (this was of course a light yellow/pinkinsh color, looking very similar to my own skin), 
I would always go for yellow. As I got older, I realized that if I were to color a white person's skin and I didn't have the "skin-color" pencil, then using a regular pink 
would probably be a better fit than yellow. Anyway, my point is that my child-mind saw yellow as the second closest color to my white skin, and I'm sure I wasn't the only one, so I definitely 
think that the argument that yellow leans close to a white skin tone rings true. And if the people coming up with emojis really wanted to make them so they couldn't be 
identified with a race/skin color, going with a color that no one has as a skin color would probably be a better way to go. Like purple. Or green. Or blue. 

So maybe that was my point of the program. If you claim that your emoji is not a racially biased color, then maybe you shouldn't go for a color that a seven year old child
would use to color skin. 

(The program will also start coloring the emojis in random if the mouse is pressed, but that's simply a feature that was added because I was playing around with it.)

![Screenshot](miniex02_screenshot.JPG)