**Mini Exercise 04 by Cecilie Frandsen**

"CAPTURE ALL"

[RunMe](https://cdn.staticaly.com/gl/ckf97/my-collection-ap2019/raw/master/MiniEx04/MiniEx04_library/sketch04/index.html) (I think both camera and mic have to be allowed for the page to run correctly)

![Screenshot](miniex04_screenshot.PNG)

Data capturing is part of our everyday lives because we use digital artefacts every single day. We can't escape it anymore. A corporation 
like Google is linked to so much of our social media, they probably have a lot more information about us than we're aware of, not only as
social groups but as individuals as well. The presence of cameras and microphones is only the obvious data capturing. The data capturing 
that happens when we use social media, online shopping, practically anything on a computer, is much less obvious and maybe more dangerous, 
at least if the information was leaked. 

My program for the capture all theme, didn't turn out the way I wanted it to. I used Winnie's face capture program as a base for my own, 
but was unable to make the ellipses at the specific points in the clmtracker interact with other objects, which was what I actually 
wanted to make my program do. But since I couldn't make it work the way I wanted, I had to figure out something else. I made the ellipse 
track only one specific place on the face (the nose), so that you essentially are able to draw with it, and then I added the button that 
enables you to clear the screen and start over. Because I didn't quite feel like there was enough going on in the program, I added another 
ellipse, which follows the mouse's position on the canvas, and changes color depending on where it is in the canvas. As I was playing 
around with this and drawing on the page, I realized it was impossible to copletely cover the ellipse from the tracker. With that 
realization, I was able to conceptualize an idea from this program. That is, the tracker-ellipse is always present, no matter how hard 
you try to cover it. I see this as a conceptualization of the fact that we in the digital age are unable to escape from data capturing. 
I lastly added the words "you can't cover the red" on the screen, in the same white color as the background so the words would be hidden, 
unless the user begins drawing and therefor uncovers them. You are essentially uncovering the hidden message, which is that we are 
constantly caught in data capturing everywhere.
