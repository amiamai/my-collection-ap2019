var x = 0;
var speed = 4;
var song;

function setup() {
  song = loadSound("403.mp3", loaded);
  createCanvas(1280, 710);
  print("Hello Winnie!");
}

//play sound
function loaded() {
  song.loop();
}

function draw() {
  //background
	col = map(mouseY,0,600,0,255);
  background(col);

  //make volume follow color change (ellipse)
  vol = map(col,0,255,0,3);
  song.setVolume(vol);

  //rectangle
  stroke(255,100,0);
  strokeWeight(10);
  noFill();
  rect(x, 200, 100, 100);

  if (x > width) {
		speed = -4;//makes rect bounce
    song.rate(1.2);//changes soundrate
    print("Now run!")
	}

  if (x < 0) {
    speed = 4;//makes rect bounce
    song.rate(1);//changes soundrate
    print("Now walk.")
  }
  x = x + speed;

  //line across (just because, idk)
  line(0, 0, 1530, 730);

  //ellipse controls background and volume
  fill
  ellipse(mouseX,mouseY,70,100);

//change background color (kind of)
if (mouseIsPressed)
  background(255,0,100,60);
}
