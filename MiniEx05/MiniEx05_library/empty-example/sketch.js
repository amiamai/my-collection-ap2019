let balls = [];
let x = 75;
let speed = 10;
let d;
let beep;
let tada;

//sounds
function preload () {
  beep = loadSound('Beep.mp3');
  tada = loadSound('TaDa.mp3');
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  //ellipse diameter
  d = 150;

}

function draw() {
  //background changing depending on where the ellipse
  //is on the y axis
	col = map(mouseY,0,windowHeight,0,255);
  background(col);

  //show balls and let them fall
  for (let i = 0; i < balls.length; i++) {
    balls[i].fall();
    balls[i].show();
  }

  //main ellipse moving side to side
  stroke(255,100,0);
  strokeWeight(10);
  noFill();
  ellipse(x, mouseY, d);

  //movement and size
  if (x > width-75) {
		speed = -10; //makes ellipse bounce
    d = d + 30; //makes ellipse bigger
    addBalls();
  }

  if (x < 75) {
    speed = 10; //makes ellipe bounce
    d = d + 30; //makes ellipse bigger
    addBalls();
  }
  x = x + speed;

//sound coordination
  if (x <= 75 || x >= width-75) {
    beep.play();
  } else {
    beep.stop();
  }

  //makes ellipse original size + celebratory sound
  if (d > 600) {
    beep.stop();
    tada.play();
    d = 150;
  }
}

function addBalls() {   //speed, xpos, ypos, size
  balls.push(new Balls(floor(random(1,5)),floor(random(0,width)), floor(0), 20));
}

class Balls {
  constructor(speed, xpos, ypos, size){
    this.x = xpos;
    this.y = ypos;
    this.r = size;
    this.speed = speed;
  }

  fall() {
    this.y = this.y + this.speed;
  }

  show() {
    fill(220,200,150);
    stroke(170,150,100);
    strokeWeight(2);
    ellipse(this.x, this.y, this.r);
  }
}
