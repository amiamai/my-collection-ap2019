**Mini Exercise 05 by Cecilie Frandsen**

[RunMe](https://glcdn.githack.com/ckf97/my-collection-ap2019/raw/master/MiniEx05/MiniEx05_library/empty-example/index.html)

![Screenshot](MiniEx05_screenshot.PNG)

**Concept:** 
The idea of the program is that whenever the main ellipse bounces off the side, the ellipse grows bigger and a smaller ellipse fall from the screen. 
A beep is also heard and when the ellipse has reached a certain size, a congratulatory sound is heard and the ellipse goes back to it's original size until it 
bounces again and the size once again starts incresing. 

The departure point of the program is when it starts running. The program begins on it's own and cannot be stopped. 

I wanted to express to myself that I was able to do something that at least had a little more sense to it, compared to the program it is based on, where things
happened for a reason and not just because I thought it looked pretty cool or wanted to try it out.I was sort of proving to myself that I actually learned 
something. I based this program on my very first mini exercise, which was really just a weird concoction of several different syntaxes that I barely understood. 
I kept the bouncing ball (changed it from a rectangle to and ellipse though) and refined it a little, although I would have like to have made it so the ellipse, 
no matter the size, would bounce when it hit the edge of the circle and not at the specific x-position I gave it, but I wasn't sure how to do it. 
I removed the song playing in the first mini exercise, as well as the line that went across the screen, and instead added beeps etc. to the program. 
As a final detail, I tried my way with the class syntax and I made small ellipses appear at the top of the screen and fall down whenver the bigger ellipse 
bounced off the sides. 

**What does it mean by programming as a practice? **
Programming as a practice is among other things, a way to see programming as not only an engingeering tool but also as a creative tool, and a tool for everyone.
It used to be something done by specialists only, but it has become a practice that can be used in many places like the context of professional arts. 
The fact that programming is something we work to make more accessible and "easier" to work with for a broader spectrum of programmers, is also part of 
programming as a practice. And if we consider the literacy of programming, and the fact that a certain amount of literacy is needed to program at all, is also 
part of what makes programming a practice. 

**To what extend do you agree the concepts that have been illustrate in below readings?** 
A point in several of these texts is the immateriality of programming, or rather how it is *not* immaterial. While when programming you are not necessarily 
making something you can hold in your hand, it is still a material. I agree with this a lot, as it is possible to interact with, work on, and in general 
because you are able to make something where there before was nothing. Bergstrom and Blackwell even talk of materiality as something purely mental: "*Material 
is in other words not manifest in some physical reality, but is a purely mental construct.*" (Bergstrom and Blackwell, 2016, p.7). And the programming that 
is done is also a big part of making things that is in our physical reality. Montford uses this example of an engineer: "*A civil engineer modeling an 
unusually designed bridge with a computer is better able to ensure that it is safe than one who must rely on earlier methods*" (Montford, 2016, p.269). 
Programming something before making a real life product can ensure that the numbers are correct, which can in this case prevent a bridge from collapsing. 
What that is done with programming can affect the physical results, which I think even further illustrates how programming is not immaterial. 

**What is the relation between programming and digital culture?**
Programming and digital culture is directly related because everything digital is programmed and programmable. There is nothing within digital culture that is 
not programmable - digital culture can't even exist without programming. And allowing people to become more educated in programming, allows them to be more 
critical of the media they're consuming, as well as giving them a different outlook on the world and becoming more active consumers. 
