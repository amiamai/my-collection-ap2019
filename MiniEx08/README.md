**Mini Exercise 08 by Cecilie Frandsen**

This mini exercise was made along with Aia Kragh. 

[RunMe](https://glcdn.githack.com/Kragh/ap2019/raw/master/miniExes/MiniEx8/index.html)

![Screenshot](SB8.JPG)

The program for this week's exercise generates poems. It works in the way that it has a few lines from some poems, which are called randomly from a JSON file 
every time the mouse is clicked. After only a few clicks, you have generated a poem. To start over and make a new poem, you press the arrow down key, which then 
removes the poem and chooses a new background from an array of images. The concept idea for this program was about time and waiting, maybe waiting endlessly or 
waiting restlessly, but not waiting that was disturbing. This is why we made the typing bubble in the corner of the screen, that was where our idea first led us. 
The typing bubble comes with a connotation of waiting endlessly, restlessly or even patiently, but the notion of time is part of the typing bubble in this aspect. 
As it is, the moving dot in the bubble only moves when the mouse goes by it, which was supposed to only be a temorary solution but ended being the end product 
because we couldn't figure out how to make it move on its own. Because we had to incorporate language/literature in the program, we looked for poems which we 
thought fit this concept well, poems that spoke of time or waiting. When choosing the images for the program, we also wanted images that seemed sort of timeless 
or endless. 

The collaborative process was quite interesting, I think Aia and I worked well together. She was very good at coming up with a concept for the program, and I then 
tried to figure out some more technical aspects. We coded the work together, and when we ran into problems both of us would search for the solution, which led us 
to figuring out the problems quicker. Particularly when writing the JSON file, it actually helped to be two people, so that while one was writing it, the other 
could make sure that every bracket or colon was in the right place. Since it was our first time trying to use a JSON file, it was very nice that it didn't take 
forever to try and make it work. 

The generated poems in the program are made by having several single lines of poetry in a collection, which is then called on randomly. When the lines appear on 
the screen, it is then possible to get two of the same lines in a row, or have the same lines several times in one paragraph. The paragraphs are also placed 
randomly, and they happen because the program has been told to go through the more lines then there actually are. Which means that it will occasionally call on a 
line that doesn't exist and doesn't write anything, which then creates the illusion of a paragraph. The three poems that are used in this program have all been 
chosen with the same concept in mind which is why all the lines essentially "fit together" despite them being from three different poems. Because the chosen 
lines are all single sentences, the grammar of the poems is not a problem. 
Because we are also working with poems, the difference between what fits together and what doesn't is not so straightforward, which is why we can get 
away with having several of the same lines in a row, because when the reader reads it as a poem and there are no real rule-set for poems, it basically always works. It's kind of like *Love Letters* 
(Christopher Strachey) in the way that even if it seems quite ridiculous when reading it, it still looks real based on our knowledge of what a poem (love letter) 
can be. 

**What is the aesthetic aspect of your program in particular to the relationship between code and language?**

The aesthetic aspect of the program became the focus of the exercise. We used language/literature in the program, but didn't think too deeply of how exactly 
the code would work as casual reading, only that what was executed by the program could be read and look the part. The aesthetic aspect of the program focuses 
quite a lot on the poem really looking like a poem, as well as expressing the time-centered concept. 

