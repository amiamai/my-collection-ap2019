let size = 25;
let spacing = 35;
let xpos = 150;
let ypos = 80;
let texty = 80;



function preload() {
 blue = loadImage("blue.jpg");
 black = loadImage("shadow.jpg");
 navy = loadImage("blue2.jpg");
 red = loadImage("red.jpg");
 brown = loadImage("brown.jpg");
 grey = loadImage("grey.jpg");
 mix = loadImage("tattoo.jpg");


 poetry = loadJSON('poetry.json');

}

function setup() {


  rectMode(CENTER);
  createCanvas(windowWidth,windowHeight);

//background
  let pictures = [blue, black, navy, red, brown, grey, mix];
  image(random(pictures), width/4, 40, 400, 550);

  fill(0);
  text("click_for_poetry\nkey_↓_refresh", width/4, 620);

//typing bubble
   fill(230, 245);
   noStroke();
   ellipse(xpos-60, ypos+27, 40, 40);
   ellipse(xpos-85, ypos+47, 18, 18);
   rect(xpos,ypos,150,80,35);


}

function draw() {
  dots();
  startover();

}


function mousePressed() {
  var poemsRandom = Math.floor(Math.random()+floor(random(3)));
  var wordsRandom = Math.floor(Math.random()+floor(random(4)));

  text(poetry.poems[poemsRandom].words[wordsRandom], width/3, texty);

texty = texty + 15;

}


function dots() {
let xpos2 = 150;

  if (mouseX > 85) {
    xpos2 = xpos2+spacing;
  }
  if (mouseX > 140) {
    xpos2 = xpos2+spacing;
  }

    //dots
    fill(200);
    noStroke();
    ellipse(xpos,ypos,size);
    ellipse(xpos-spacing,ypos,size);
    ellipse(xpos+spacing,ypos,size);

    //moving dot
    fill(150);
    noStroke();
    ellipse(xpos2-spacing,ypos,size);
}

function startover() {
  let pictures = [blue, black, navy, red, brown, grey, mix];

  if (keyIsDown(DOWN_ARROW)) {
    clear();

    //startover with new image and text
    texty = 80;
    image(random(pictures), width/4, 40, 400, 550);
    //typing bubble
    fill(230, 245);
    noStroke();
    ellipse(xpos-60, ypos+27, 40, 40);
    ellipse(xpos-85, ypos+47, 18, 18);
    rect(xpos,ypos,150,80,35);
  }


}
