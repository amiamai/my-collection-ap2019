//Mini Exercise 03 by Cecilie Frandsen
//Make your own throbber

let fade;
let belly;
let ears;
let inEars;
let head;
let blackEyes;
let eyes;
let mouth;
let pawsBottom;
let pawsMiddle;
let right;

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(220);
  frameRate(8);
}

function draw() {
  //line in the middle of the screen
  fill(0);
  rect(width/2,0,2,windowHeight)

  //extra "background" to make throbber effect
  fill(200,80);
  noStroke();
  rect(0,0,width,height);

  //throbber functions
  pandaOne(10);
  pandaTwo(30);
}

//Panda Throbber going up and down the screen
function pandaOne(num) {
  right = width/2

  //belly
  push();
    translate(right,270);
    let y = (0,windowHeight)/num*(frameCount%num);
    belly = 255;
    fill(belly);
    ellipse(300,y,250,300);
    fill(220)
    ellipse(300,y,210,210);
  pop();

  //ears
  push();
    translate(right,30);
    ears = 255;
    fill(ears);
    ellipse(230,y,60,60);
    ellipse(370,y,60,60);
    //in-ears
    inEars = 150;
    fill(inEars);
    ellipse(230,y,40,40);
    ellipse(370,y,40,40);
  pop();

  //head
  push();
    translate(right,100);
    head = 240;
    fill(head);
    ellipse(300,y,200,200);
    nose = 80;
    fill(nose);
    ellipse(300,y,20,15);
  pop();

  //eyes
  push();
    translate(right,70);
    blackEyes = 50;
    fill(blackEyes);
    ellipse(260,y,55,55);
    ellipse(340,y,55,55);
    eyes = 255
    fill(eyes);
    ellipse(260,y,25,25);
    ellipse(340,y,25,25);
  pop();

  //mouth
  push();
    translate(right,160);
    mouth = 180;
    fill(mouth);
    ellipse(300,y,40,40);
  pop();

  //paws
  push();
    translate(right,400);
    pawsBottom = 150;
    fill(pawsBottom);
    ellipse(230,y,60,80);
    ellipse(370,y,60,80);
  pop();

  push();
    translate(right,260);
    pawsMiddle = 100
    fill(pawsMiddle);
    ellipse(210,y,60,80);
    ellipse(390,y,60,80);
  pop();
}

//Panda Throbber fading
function pandaTwo(num) {
  translate(80,30);

  let fade = (0,255)/num*(frameCount%num)

    //belly
    fill(belly,fade);
    ellipse(200,270,250,300);
    fill(220,fade);
    ellipse(200,270,210,210);

    //ears
    fill(ears,fade);
    ellipse(130,30,60,60);
    ellipse(270,30,60,60);
    //in-ears
    fill(inEars,fade);
    ellipse(130,30,40,40);
    ellipse(270,30,40,40);

    //head
  	fill(head,fade);
    ellipse(200,100,200,200);
    //nose
    fill(nose);
    ellipse(200,100,20,15);

    //eyes
    fill(blackEyes,fade);
    ellipse(160,70,55,55);
    ellipse(240,70,55,55);
    fill(eyes,fade);
    ellipse(160,70,25,25);
    ellipse(240,70,25,25);

    //mouth
    fill(mouth,fade);
    ellipse(200,160,40,40);

    //paws
    fill(pawsBottom,fade);
    ellipse(130,400,60,80);
    ellipse(270,400,60,80);

    fill(pawsMiddle,fade);
    ellipse(110,260,60,80);
    ellipse(290,260,60,80);

    inpaw = 255;
    fill(inpaw,fade);
    //right paw
    ellipse(130,412,40,40);
    ellipse(115,385,12,15);
    ellipse(130,375,12,15);
    ellipse(145,385,12,15);
    //left paw
    ellipse(270,412,40,40);
    ellipse(255,385,12,15);
    ellipse(270,375,12,15);
    ellipse(285,385,12,15);
}
