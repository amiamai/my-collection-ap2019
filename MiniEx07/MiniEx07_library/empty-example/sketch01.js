let x = 0;
let y = 0;
let spacing = 25;
let boxes = [];

function setup() {
  createCanvas(1540, 760);
  background(0);
  //tiny black boxes
  for (let i = 0; i < 6000; i++) {
    boxes[i] = {
      x: random(25,width-25),
      y: random(25,height-25),
      display: function () {
        noStroke();
        fill(0);
        rect(this.x,this.y,5,5);
      }
    }
  }
}

function draw() {
//red and green boxes and 10PRINT making different patterns
  if (random(1) < 0.7) {
    noStroke();
    fill(255,0,0);
    rect(x+spacing,y+spacing,spacing,spacing);
    stroke(255);
    strokeWeight(4);
    line(x,y,x+spacing,y+spacing);
  } else {
    noStroke();
    fill(0,255,0);
    rect(x+spacing,y+spacing,spacing,spacing);
    strokeWeight(4);
    stroke(255);
    line(x,y+spacing,x+spacing,y);
  }
  x = x + spacing;
  //moving to the next line
  if (x > width-50) {
    x = 0;
    y = y + spacing;
  }
  if (x < width-50 && y > height-50) {
    x = 0;
    y = 0;
      for (let i = 0; i < boxes.length; i++) { //showing tiny black boxes
        boxes[i].display();
    }
  }
}
