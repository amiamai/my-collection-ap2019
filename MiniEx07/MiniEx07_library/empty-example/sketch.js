let x;
let inc = 0.02;
let start =0;

function setup () {
  createCanvas(windowWidth,windowHeight);

}

function draw () {
  background(0);
  // for (x = 0; x < width; x++) {
  // stroke(255);
  // point(x,random(height));
  // }
  beginShape();
  let xoff = start;
  for (x = 0; x < width; x++) {
    stroke(255);
    noFill();
    let y = noise(xoff)*height;
    vertex(x,y);

    xoff += inc;
  }
  endShape();

  start += inc;
}
