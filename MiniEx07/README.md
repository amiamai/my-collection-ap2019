**MiniEx07 by Cecilie Frandsen**

[RunME](https://glcdn.githack.com/ckf97/my-collection-ap2019/raw/master/MiniEx07/MiniEx07_library/empty-example/index.html)

![Screenshot](MiniEx07_screenshot.PNG)

Warning: a very frustrated Cecilie wrote this, please don't take it to heart if I sound positively rude af. 

I noticed that one of the objectives for this week's exercise is to strenghten our computational use of loops, so I'm just going to state this right now that the 
for-loop is still hard for me and I don't think this week necessarily strengthened my use of it. The workings of the for-loop and implementing it in the programs 
is continuously hard for me, as well as the use of the for-loop along with an array. I assume there is some sort of vital piece of information that I've simply 
missed at some point about how and why it is the way it is, but I can't seem to ever fully understand what I'm doing with it. 

Now that that's been said, let's move on to this program. The objective of a rule-based generative program wasn't exactly as easy as I had hoped it would be. 
I wanted to do something cool with lines, but I'm not really good at working with lines yet, nor how to make them move, even less so how to make it look random 
or coincidental - on purpose. Basically, I was struggling. Instead, I decided to take inspiration from the 10PRINT program Dan Shiffman made (based off the 
program made on Commodore 64, which you can see in my program as well. 
I actually did this exercise the "wrong" way, and I made a program before I considered what my rules should be, mainly because I had difficulty even just 
thinking about how to make rules from the real world, from my mind, and implement them into code. Nevertheless, here are my rules:

1) Of numbers between 0 and 1, if the number is less than 0.7, draw a red rectangle. Otherwise draw a green rectangle.

2) If the rectangle (regardless of color) is nearly outside of the right side of the canvas, start a new line of rectangles underneath the existing one. 
Go back to the first rectangle and start over if there is not room for another line of rectangles at the bottom of the canvas. 

3) When the rectangles have reached the bottom of the canvas, display thousands of tiny black rectangles. 

My three rules are quite obviously written in regards to how the program works, but there are three of them and they tell you how the program works, so that's that. 
The addition of the black rectangles is essentially because I had to use a for-loop in one way or the other, so I decided to implement them at the "end" of the 
program, so that when the program runs once again, this time over the already drawn rectangles, it will be more obvious that the colors are now different than 
the last sketch. I kept the 10PRINT in the program as well so that the program looked more dynamic I guess. The lines making up the 10PRINT is slightly 
offset of the rectangles and the reason is that I honestly just didn't feel like going into the code again and translating their starting point, as well as the 
fact that it makes it more obvious that the new rectangle-pattern is slightly different than the one before. I also wasn't sure how exactly to change where they 
start a new line, since I didn't really understand why it was different from the rectangles in the first place, so I kept it the way it was. As you can tell, lots of 
things about my program confuses me and I keep them because I don't bother changing them. So... minus 10 points to me for lack of productivity and learning. 

I use only the random syntax throughout the program. Although I did experiment with the noise syntax before beginning work on what I was going to actually "hand in" 
to see how the syntax worked, I decided not to use it in this program because I thought it was more complex, and it would take longer for me to implement it in 
the program. Generally, all of my decisions are based on how efficient it will be in my programming process. 

This exercise gave insight into understanding what generativity and automatism is, in the way that I now see it as something very man-made and not as random 
or coincidental as I might have thought it to be before. It works automatically because I tell it what to do whenever it reaches what I (or humans in general) 
conceive as the "end", and I tell it to do something again. It generates something "new" because I tell it to, but I don't give the program 100% specifics, 
rather I give it a range of "choices" and then makes the program choose randomly within said "choices". Essentially, it's orchestrated randomness by me, and 
while I may not be able to predict how the program specifically will change at all times, I have a general idea of what it will end up looking like because I 
set the boundaries and limitations of the program. It's like organized chaos. 

As you can probably tell, this week I've fallen into the pit of "everything is hard and I hate it", and I'm pretty sure my frustrations are reflected in 
how I'm writing this readme. My lack of understanding in why certain syntaxes work the way they do, and the frustrations present when I can't make them work 
the same way is rubbing off on my will to do it, like "I can't do it right, so why even bother" is sort of where I'm at. Hope that's alright for this week, I'll 
work on doing better for the next exercise. 